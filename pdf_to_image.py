import pdf2image 
import pandas as pd 
from PyPDF2 import PdfFileReader
import glob
import os 
from tqdm import tqdm
from pdf2image import convert_from_path, convert_from_bytes
from pdf2image.exceptions import (PDFInfoNotInstalledError,PDFPageCountError,PDFSyntaxError)


def read_json(json_file):
	"""
	This module helps us to read and extract the information from json file
	"""
	with open(json_file,'r') as f:
		data = json.load(f)
		print(data)
	return data

def create_folder(dir_path):
	if not os.path.exists(path):
		os.mkdir(path)
	else:
		continue

def convert_pdf_img(data):
	create_folder(data['output_dir'])
	filename_list = glob.glob(data['input_dir'])
	for i in tqdm(range(len(filename_list))):
    	filename = str(filename_list[i].split("/")[-1].split(".pdf")[0])
    	print(filename)
    	try:
        	images = convert_from_path(filename_list[i],dpi=200)
        	for index, val in enumerate(images):
            	val.save(out_dir+'/'+filename+'_'+str(index)+".jpg", "JPEG")
    	except:
            continue

if __name__ == __main__:
	data = read_json(json_file)
	convert_pdf_img(data)
	